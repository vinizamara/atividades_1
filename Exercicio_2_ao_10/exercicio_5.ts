//Cálculo de salário
/*
Faça um programa que receba o salário de um funcionário e
o percentual de aumento, calcule e mostre o valor do
aumento e o novo salário.
*/

namespace exercicio_5
{
    //Entrada de dados
    const salario_antigo = 1000;
    const aumento = 10;

    //Processamento
    let valor_do_aumento: number;
    let salario_novo: number;

    valor_do_aumento = salario_antigo * aumento/100;
    salario_novo = salario_antigo + valor_do_aumento;

    //Saída de dados
    console.log(`O valor do aumento é: R$${valor_do_aumento}`)
    console.log(`O seu novo salário é: R$${salario_novo}`)
}
