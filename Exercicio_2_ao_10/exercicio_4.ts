//Cálculo de salário
/*
Faça um programa que receba o salário de um funcionário,
calcule e mostre o novo salário, sabendo-se que este sofreu
um aumento de 25%.
*/

namespace exercicio_4
{
    //Entrada de dados
    const salario_antigo = 1000;

    //Processamento
    let novo_salario: number;
    
    novo_salario = salario_antigo * 1,25;
    //ou podemos escrever como
    novo_salario = salario_antigo + ( salario_antigo * 25/100);

    //Saida de dados
    console.log(`Seu novo salário é de R$${novo_salario}`);
}
