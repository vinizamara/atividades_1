//Cálculo de salário
/*
Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/

namespace exercicio_8
{
    //Entrada de dados
    const deposito = 1000;
    const juros = 10;

    //Processamento
    let rendimento: number;

    rendimento = deposito + (deposito * juros/100);

    let total: number;

    total = rendimento + deposito;

    //Saída de dados
    console.log(`Seu redimento foi de: R$${rendimento} \nJá o seu valor total foi de: R$${total}`);
}
