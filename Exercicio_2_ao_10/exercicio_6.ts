//Cálculo de salário
/*
Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o salário a receber, sabendo-se
que esse funcionário tem gratificação de 5% sobre o salário
base e paga imposto de 7% sobre o salário-base.
*/

namespace exercxicio_6
{
    //Entrada de dados
    const salario_base = 1000;

    //Processamento
    let salario_receber: number;
    
    salario_receber = salario_base + (salario_base * 5/100) - (salario_base * 7/100);

    //Saída de dados
    console.log(`Seu salário a receber é: R$${salario_receber}`);
}
