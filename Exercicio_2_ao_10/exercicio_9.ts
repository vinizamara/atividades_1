//Área do triângulo
/*
Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.
*/

namespace exercicio_9
{
    //Entrada de dados
    const altura = 10;
    const base = 5;

    //Processamento
    let area: number;

    area = (base * altura) / 2;

    //Saída de dados
    console.log(`A área do triângulo é igual a ${area}`);
}
