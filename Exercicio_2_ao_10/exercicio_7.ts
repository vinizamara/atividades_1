//Cálculo de salário
/*
Faça um programa que receba o salário-base de um
funcionário, calcule e mostre o seu salário a receber, sabendo-
se que esse funcionário tem gratificação de R$50,00 e paga
imposto de 10% sobre o salário-base.
*/

namespace exercicio_7
{
    //Entrada de dados
    const salario_base = 1000;

    //Processamento
    let salario_receber: number;

    salario_receber = salario_base + 50 - (salario_base * 7/100);

    //Saída de dados
    console.log(`Seu salário a receber é: R$${salario_receber}`);
}
