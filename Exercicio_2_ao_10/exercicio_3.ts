//Média Ponderada
/*
Faça um programa que receba três notas, calcule e mostre a
média ponderada entre elas.
*/

namespace exercicio_3
{
    //Entrada de dados
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 1;
    nota2 = 5;
    nota3 = 8;
    peso1 = 1;
    peso2 = 4;
    peso3 = 5;

    //Processamento
    let media: number;
    
    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    //Saída de dados
    console.log(`Sua média ponderada é: ${media}`)
}
