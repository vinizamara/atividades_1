//Área de um círculo
/*
Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/

namespace exercicio_10
{
    //Entrada de dados
    const raio = 5;

    //Processamento de dados
    let area: number;

    area = Math.PI * (raio ** 2);

    //Saída de dados
    console.log(`A área do círculo é igual a ${area}`);
}
