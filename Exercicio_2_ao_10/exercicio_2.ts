//Média Aritmética
/*
Faça um programa que receba três notas, calcule e mostre a
média aritmética entre elas.
*/

namespace exercicio_2
{
    //Entrada de dados
    let nota1, nota2, nota3: number;

    nota1 = 1;
    nota2 = 2;
    nota3 = 3;

    //Processamento
    let media: number;

    media = (nota1 + nota2 + nota3) / 3;

    //Saída de dados
    console.log("Sua média aritmética é: " + media);
    //ou podemos escrever como:
    console.log(`Sua média aritmética é: ${media}`);
}
